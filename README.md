# Projeto Pandora - API 

A API é o componente do projeto responsável pelo backend da aplicação. Nela estão definidos todos os métodos de acesso ao banco de dados não relacional MongoDB. Também estão definidos os modelos das entidades que serão registradas no banco de dados.



## Models(Modelos)

São as classes que representam as estruturas de dados que serão armazenadas no banco. 


### Administrador

-----------------------------------------
|       Atributos        |       Tipo    |
| -----------------------|---------------|
|    id                  |    String     |
|    nome                |    String     | 
|    login               |    String     |
|    senha               |    String     |
|   tipoDeAcesso         |    String     |
------------------------------------------

##### Rotas

Metodo POST:

```
 http://host:porta/pandora/administrador   *** ou *** https://dominio/pandora/administrador
```

Este metodo recebe como parâmetro um json com as seguintes informações:

```json
{
  "nome": "Nome do Administrador",
  "login": "email do administrador",
  "senha": "senha do administrador",
  "tipoDeAcesso": "Administrador" -> Valor Fixo
}
```

Metodo GET:

```
   http://host:porta/pandora/administrador   *** ou ***  https://dominio/pandora/administrador  
```

Retorna um json com os seguintes campos(no app):

```json
{
  "id"  : "Id do Administrador",
  "nome": "Nome do Administrador",
  "login": "email do administrador",
  "senha": "senha do administrador",
  "tipoDeAcesso": "Administrador"
}
```

Para realizar o login do administrador:

```
   http://host:porta/pandora/administrador/login/:login/:senha   *** ou ***  https://dominio/pandora/administrador/login/:login/:senha  
```


Metodo PUT:
 
```
   http://host:porta/pandora/administrador/:id   *** ou ***  https://dominio/pandora/administrador/:id  
```

Metodo DELETE:

```
   http://host:porta/pandora/administrador/:id   *** ou ***  https://dominio/pandora/administrador/:id  
```




### Aluno

-----------------------------------------
|       Atributos        |       Tipo    |
| -----------------------|---------------|
|    id                  |    String     |
|    nome                |    String     | 
|   matricula            |    String     |
|    turma               |    String     |
|    login               |    String     |
|    senha               |    String     |
|   tipoDeAcesso         |    String     |
|    limites             |    Object     |
|    derivadas           |   Object(Map) |
|    integrais           |   Object(Map) |
|   questoesRespondidas  |   [String]    | 
------------------------------------------

##### Rotas

Metodo POST:

```
 http://host:porta/pandora/aluno   *** ou *** https://dominio/pandora/aluno
```

Este metodo recebe como parâmetro um json com as seguintes informações:

```json
{
  "nome": "Nome do Aluno",
  "matricula":"matricula do aluno",
  "turma":"turma do aluno",
  "login": "email do aluno",
  "senha": "senha do aluno",
  "tipoDeAcesso": "Aluno" -> Valor Fixo,
  "limites":{"acertos": 0, "erros": 0},
  "derivadas":{"acertos": 0, "erros": 0},
  "integrais":{"acertos": 0, "erros": 0},
  "questoesRespondidas": []
}
```

Metodo GET:

```
   http://host:porta/pandora/aluno   *** ou ***  https://dominio/pandora/aluno  
```

Retorna um json com os seguintes campos(no app):

```json
{
  "id"  : "Id do Aluno",
  "nome": "Nome do Aluno",
  "matricula":"matricula do aluno",
  "turma":"turma do aluno",
  "login": "email do aluno",
  "senha": "senha do aluno",
  "tipoDeAcesso": "Aluno" -> Valor Fixo,
  "limites":{"acertos": 0, "erros": 0},
  "derivadas":{"acertos": 0, "erros": 0},
  "integrais":{"acertos": 0, "erros": 0},
  "questoesRespondidas": []
}
```

Para realizar o login do aluno:

```
   http://host:porta/pandora/aluno/login/:login/:senha   *** ou ***  https://dominio/pandora/aluno/login/:login/:senha  
```

Para recuperar o aluno por ID:

```
   http://host:porta/pandora/aluno/:id   *** ou ***  https://dominio/pandora/aluno/:id  
```


Metodo PUT:
 
```
   http://host:porta/pandora/aluno/:id   *** ou ***  https://dominio/pandora/aluno/:id  
```

Metodo DELETE:

```
   http://host:porta/pandora/aluno/:id   *** ou ***  https://dominio/pandora/aluno/:id  
```
  
  
### Tarefas


-----------------------------------------
|       Atributos        |       Tipo    |
| -----------------------|---------------|
|    label               |    String     |
|    state               |    Boolean    |
|    questions           |    [String]   |
|    content             |      String   |
------------------------------------------

##### Rotas

Metodo POST:

```
 http://host:porta/pandora/tarefas  *** ou ***  https://dominio/pandora/tarefas

```

Este metodo recebe como parâmetro um json com as seguintes informações:

```json
   {
    label: "Dia ao qual a tarefa pertence",
    state: false,
    questions: [],
    content: "Assunto ao qual a tarefa se refere(Limites, Derivadas ou Integrais)"   
   }

```

Metodo GET:

```
    http://host:porta/pandora/tarefas   *** ou ***  https://dominio/pandora/tarefas  
```

Para recuperar uma tarefa pelo ID:

```
    http://host:porta/pandora/tarefas/:id   *** ou ***  https://dominio/pandora/tarefas/:id  
```

Retorna um json com os seguintes campos:


```json
    "label": "Dia ao qual pertence a tarefa",
    "state": false,
    "content": "Conteúdo da Tarfefa",
    "questions": [id das questoes vinculadas a tarefa]
```
 
  
Metodo PUT:

```
    http://host:porta/pandora/tarefas/:id   *** ou ***  https://dominio/pandora/tarefas/:id  
```
  
Metodo DELETE:
  
```
    http://host:porta/pandora/tarefas/:id   *** ou ***  https://dominio/pandora/tarefas/:id  
```
  
### Questao

-----------------------------------------
|       Atributos        |       Tipo    |
| -----------------------|---------------|
|    taskNumber          |    Number     |
|    state               |    Boolean    |
|    questions           |    [String]   |
|    content             |      String   |
------------------------------------------

##### Rotas

Metodo POST:

```
 http://host:porta/pandora/questoes  *** ou ***  https://dominio/pandora/questoes
```

Este metodo recebe como parâmetro um json com as seguintes informações:

```json
   {
    taskNumber: "Número da tarefa a qual pertence a questao",
    urlCommands: Object com os comandos e/ou gráficos da questao => {comando: "", grafico: " " },
    urlAlternatives: Object com as urls das alternativas das questões,
    answer: Object com a(s) resposta(s) da(s) alternativa(s),
    solvedBy: "Array com os ids dos alunos que resolveram a questão",
    tipo: Questão Objetiva ou Escrita,
    content: Conteúdo ao qual a questão se refere,
    info: Object com o número de erros e acertos da questão
    

   }

```

Metodo GET:

```
    http://host:porta/pandora/questoes   *** ou ***  https://dominio/pandora/questoes  
```

Metodo PUT:

```
    http://host:porta/pandora/questoes/:id   *** ou ***  https://dominio/pandora/questoes/:id  
```


Metodo GET by ID:

```
    http://host:porta/pandora/questoes/:id   *** ou ***  https://dominio/pandora/questoes/:id  
```



