const Questao = require('../models/Questao');


module.exports = {
    async index(req, res) {
        
        try{
            const questions = await Questao.find();
            return res.json(questions);
   
         }catch(e){
           return res.json({"error": e});
        }
    },

    async store(req, res) {
          try{
            question = await Questao.create(req.body);
            return res.json({"sucess":"Questão cadastrada com sucesso!"});
          }catch(e){
            return res.json({"error": e});
          }  
    },
 
    async update(req, res) {
        const questao = await Questao.findOneAndUpdate({_id: req.params.id}, req.body, {
            new: true
        });

        return res.json(questao);
    },

    async destroy(req, res) {
      const questao = await Questao.findOneAndRemove(req.params.id);
      return res.send(); 
    },

    async show(req, res) {
        await Questao.findOne({ _id: req.params.id }, (err, ques) => {
            if (err) {
                return res.json({"msg":"Questao não encontrada" });
            }
            return res.json(ques); 
        });
    }

};

