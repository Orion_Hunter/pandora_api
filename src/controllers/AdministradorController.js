const Administrador = require('../models/Administrador');


module.exports = {
    async index(req, res) {
        
        try{
            const administradores = await Administrador.find();
            return res.json(administradores);
   
         }catch(e){
           return res.json({"error": e});
        }
    },

    async store(req, res) {
          try{
            administrador = await Administrador.create(req.body);
            return res.json({"sucess":"Administrador cadastrado com sucesso!"});
          }catch(e){
            return res.json({"error": e});
          }  
    },
    async update(req, res) {
        const administrador = await Administrador.findOneAndUpdate(req.params.id, req.body, {
            new: true
        });

        return res.json(administrador);
    },

    async destroy(req, res) {

      const administrador = await Administrador.findOneAndRemove(req.params.id);
      return res.send(); 
    },

    async show(req, res) {
        
           
        await Administrador.findOne({ login: req.params.login }, (err, user) => {
            if (err) {
                return null;
            }

            if (user && user.senha == req.params.senha) {
                return res.json(user);
            } else {
                return res.json(null);                 
            }});
    }

};

