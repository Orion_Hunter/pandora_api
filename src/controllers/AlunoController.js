const Aluno = require('../models/Aluno');


module.exports = {
    async index(req, res) {

        try {
            const aluno = await Aluno.find();
            return res.json(aluno);

        } catch (e) {
            return res.json({ "error": e });
        }
    },

    async store(req, res) {
        await Aluno.findOne({ login: { $eq: req.body.login}, matricula: { $eq: req.body.matricula}}, (err, aluno) => {
           // console.log(aluno.toString());           
            if(err){
                return null;
            }
            if (aluno){
               //if(aluno.login == req.body.login || aluno.matricula == req.body.matricula){
                return res.json({ "error": "O login digitado já está sendo utilizado ou o aluno já foi cadastrado!"});               
              // }
               
            }else{
                try {
                    aluno = Aluno.create(req.body);
                    return res.json({ "sucess": "Aluno cadastrado com sucesso!" });
                } catch (e) {
                    return res.json({ "error": e });
                }
            }
        })
    },
    async update(req, res) {
        const aluno = await Aluno.findOneAndUpdate(req.params.id, req.body, {
            new: true
        });

        return res.json(aluno);
    },

    async destroy(req, res) {

        const aluno = await Aluno.findOneAndRemove(req.params.id);
        return res.send();
    },

    async show(req, res) {


        await Aluno.findOne({ login: { $eq: req.params.login }, senha: { $eq: req.params.senha }  }, (err, user) => {
            if (err) {
                return res.json(null);
            }

            if (user) {
                return res.json(user);
            } else {
                return res.json(null);
            }
        });
    },

   
    async showById(req, res){
        await Aluno.findOne({  _id: { $eq: req.params.id } } , (err, aluno) => {
             if(err){
                 return res.json(null);
             }

             if(aluno){
                 return res.json(aluno);
             }else{
                return res.json(null);
             }
        }); 
    }


};

