const Tarefas = require('../models/Tarefas');


module.exports = {
    async index(req, res) {
        
        try{
            const tarefas = await Tarefas.find();
            return res.json (tarefas);
   
         }catch(e){
           return res.json({"error": e});
        }
    },

    async store(req, res) {
          try{
  
         tarefas = await Tarefas.create(req.body);
            return res.json({"sucess":"Atividade cadastrada com sucesso!"});
          }catch(e){
            return res.json({"error": e});
          }  
    },
    async update(req, res) {
        const tarefas = await Tarefas.findOneAndUpdate({_id: req.params.id}, req.body, {
            new: true
        });

        return res.json(tarefas);
    },

    async destroy(req, res) {

      const tarefas = await Tarefas.findOneAndRemove(req.params.id);
      return res.send(); 
    },

    async show(req, res) {
      
        await Tarefas.findOne({ _id: req.params.id }, (err, tarefa) => {
            if (err) {
                return res.json(err);
            }
               return res.json(tarefa);
        });
      
    }

};

