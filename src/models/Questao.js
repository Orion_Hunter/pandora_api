const mongoose = require('mongoose');




const QuestaoSchema = new mongoose.Schema({

    taskNumber: Number,
    urlCommands: Map,
    urlAlternatives: Map,
    answer: Map,
    solvedBy: [String],
    tipo: String,
    content: String,
    info: Map
});




module.exports = mongoose.model('Questao', QuestaoSchema);