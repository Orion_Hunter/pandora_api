const mongoose = require('mongoose');


const TarefasSchema = new mongoose.Schema({
    label: String,
    state: Boolean,
    questions: [String],
    content: String        
});


module.exports = mongoose.model('Tarefas', TarefasSchema);