const mongoose = require('mongoose');


const AdministradorSchema = new mongoose.Schema({
     nome: String,
     login: String,
     senha: String,
     tipoDeAcesso: String 
    
});


module.exports = mongoose.model('Administrador', AdministradorSchema);