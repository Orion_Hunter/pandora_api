const mongoose = require('mongoose');



const AlunoSchema = new mongoose.Schema({
    nome: String,
    matricula: String,
    turma: String,
    login: String,
    senha: String,
    tipoDeAcesso: String,
    pontos: Number,
    limites: Map,
    derivadas: Map,
    integrais: Map,
    questoesRespondidas: [String] 
});


module.exports = mongoose.model('Aluno', AlunoSchema);