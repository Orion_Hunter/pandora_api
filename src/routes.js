const express = require('express');
const routes = express.Router();
//const multer = require('multer');
//const multerConfig = require("./config/multer");

const AdministradorController = require('./controllers/AdministradorController');
const AlunoController = require('./controllers/AlunoController');
//const LimitsController = require('./controllers/LimitsController');
const TarefasController = require('./controllers/TarefasController');
const QuestaoController = require('./controllers/QuestaoController');

routes.get("/pandora",(req, res) => {
     res.send({
         "title": "Pandora API",
         "version" : "1.0.0"
     });
});

routes.post("/pandora/administrador" , AdministradorController.store);
routes.get("/pandora/administrador" , AdministradorController.index);
routes.put("/pandora/administrador/:id", AdministradorController.update);
routes.delete("/pandora/administrador/:id", AdministradorController.destroy);
routes.get("/pandora/administrador/login/:login/:senha", AdministradorController.show);


routes.post("/pandora/aluno" , AlunoController.store);
routes.get("/pandora/aluno" , AlunoController.index);
routes.put("/pandora/aluno/:id", AlunoController.update);
routes.delete("/pandora/aluno/:id", AlunoController.destroy);
routes.get("/pandora/aluno/login/:login/:senha", AlunoController.show);
routes.get("/pandora/aluno/:id", AlunoController.showById);


routes.post("/pandora/tarefas", TarefasController.store);
routes.get("/pandora/tarefas", TarefasController.index);
routes.get("/pandora/tarefas/:id", TarefasController.show);
routes.put("/pandora/tarefas/:id", TarefasController.update);
routes.delete("/pandora/tarefas/:id", TarefasController.destroy);

routes.post("/pandora/questoes", QuestaoController.store);
routes.get("/pandora/questoes", QuestaoController.index);
routes.put("/pandora/questoes/:id", QuestaoController.update)
routes.get("/pandora/questoes/:id", QuestaoController.show);

module.exports = routes;