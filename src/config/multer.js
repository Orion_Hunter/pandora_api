const multer = require('multer');
const path = require('path');
const crypto = require('crypto');
const fs = require('fs');
const multerS3 = require('multer-s3');
const aws = require('aws-sdk');




const storageTypes = {
    local: multer.diskStorage({
        destination: (req, file, cb) => {
            const {number, option} = req.body       
            var place = path.resolve(__dirname, '..','..','tmp','uploads/'+String(number)+option); 
            if(!fs.existsSync(place)){
                fs.mkdirSync(place);   
            }

            cb(null, place);
        },
        filename: (req, file, cb) => {
            crypto.randomBytes(16, (err, hash) => {
                 if(err) cb(err);
                  
                 file.key = `${hash.toString('hex')} - ${file.originalname}`;
                 const {number, option} = req.body       
                 var place = path.resolve(__dirname, '..','..','tmp','uploads/'+String(number)+option);
                 
                 req.body["urlCommand"] = "tmp/uploads/"+String(number)+option+"/"+file.key;
                 cb(null, file.key);
            });
        }
    }),
    s3: multerS3({
        s3: new aws.S3(),
        bucket: process.env.BUCKET_NAME,
        contentType: multerS3.AUTO_CONTENT_TYPE,
      
        acl: 'public-read',
        key: (req, file, cb) => {
            crypto.randomBytes(16, (err, hash) => {
                if(err) cb(err); 
                const fileName = `${hash.toString('hex')} - ${file.originalname}`;
                cb(null, fileName);
           });
        }

    })
};

module.exports = {
    dest: path.resolve(__dirname, '..','..','tmp','uploads'),
    storage: storageTypes[process.env.STORAGE_TYPE],
    limits: {
        fileSize: 5 * 1024 * 1024,    
    },
    fileFilter: (req, file, cb) => {
        const allowedMimes = [
           'image/jpeg',
           'image/pjpeg',
           'image/png',
           'image/gif'
        ];

        if(allowedMimes.includes(file.mimetype)) {
            cb(null, true);
        }else{
            cb(new Error("Invalid file type."));
        }
    }
};