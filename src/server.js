require("dotenv").config();

const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const routes = require('./routes');


const app = express();




app.use(cors());
app.use(express.json());

mongoose.connect(
    process.env.DATABASE_URL,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
    }
);

app.use(routes);

var port = process.env.PORT || 3000;
app.listen(port);